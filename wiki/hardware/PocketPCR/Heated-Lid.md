# Heated lid for the PocketPCR

Upstream repository, updates, and documentation: [https://gitlab.com/pipettin-bot/forks/labware/PocketPCR](https://gitlab.com/pipettin-bot/forks/labware/PocketPCR)

Original PocketPCR by GaudiLabs: [https://github.com/GaudiLabs/PocketPCR](https://github.com/GaudiLabs/PocketPCR)

Changes:

- Software: Heated lid with PID control. Partial duplication of the code, indicators on the display, and other minor changes. The temperature is fixed at 102°C, and reaching it is not required to run the PCR.
- Hardware: Soldered NTC thermistor and cable connector to the lid.
- Power supply: power draw peaks at 5V 4A (instead of 2A). This could be limited in software, as the lid does not need to warm up as fast.

![heated_lid.png](images/heated_lid.png)

## Assembly

BOM:

- 4-pin cable connector with wires.
- 10K B3950 NTC thermistor (Mouser: 815-NTC603103J3950FT / Abracon ABNTC-0603-103J-3950F-T).
- Solder and soldering iron. A "hot plate" can be useful.

Steps:

1. Add solder to the small pads on the PocketPCR lid. We used a fancy hot plate, but a regular soldering iron can be used instead.
2. Solder the SMD thermistor to the pads. You can test that it is working by measuring the resistance as it cools.
3. Solder the cable connector to the PocketPCR, and identify the pads on the lid where each wire must connect.
4. Solder the signal wires to the corresponding pads on the lid (closest to the thermistor).
5. Solder the Vcc and GND wires to the corresponding pads on the lid (closest to the lid's edge).
6. Place some electrical tape over the thermistor. This will keep the lid's metal spring from accidentally shorting the thermistor circuit.

![IMG_0319.jpg](images/IMG_0319.jpg)
> Solder wires to the four remaining pads (red/black = Vcc/GND; blue/yellow = signal).

## License

This project is licensed under the terms of the GNU GPL v3+ license. Authorship details and license text available at the project's home repository: [https://gitlab.com/pipettin-bot/forks/labware/PocketPCR/-/blob/master/LICENSE](https://gitlab.com/pipettin-bot/forks/labware/PocketPCR/-/blob/master/LICENSE?ref_type=heads).
